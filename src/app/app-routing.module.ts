import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'bikes/list',
    loadChildren: () => import('./views/bike-list-page/bike-list-page.module').then(m => m.BikeListPageModule)
  },
  {
    path: 'bike-details/:id/view',
    loadChildren: () => import('./views/bike-detail-page/bike-detail-page.module').then(m => m.BikeDetailPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
