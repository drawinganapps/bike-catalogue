import {Component, EventEmitter, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {BehaviorSubject, Subject} from "rxjs";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  @Output() searchText: EventEmitter<string> = new EventEmitter<string>();

  searchInput: FormControl = new FormControl('');

  constructor() {
  }

  onSearchBike(): void {
    this.searchText.emit(this.searchInput.value);
  }
}
