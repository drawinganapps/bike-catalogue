import {Component, OnInit} from '@angular/core';
import {take} from "rxjs";
import {BikeModel} from 'src/app/core/bikes/models/bike.model';
import {BikeService} from 'src/app/core/bikes/service/bike.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-bike-list-container',
  templateUrl: './bike-list-container.component.html',
  styleUrls: ['./bike-list-container.component.scss']
})
export class BikeListContainerComponent implements OnInit {
  bikes: BikeModel[] = [];

  constructor(private bikeService: BikeService, private router: Router) {
  }

  ngOnInit(): void {
    this.bikeService.getAllBikes().pipe(
      take(1)
    ).subscribe(bikeList => {
      this.bikes = bikeList;
    })
  }

  searchBike(text: string): void {
    this.bikeService.findBikes(text).pipe(
      take(1)
    ).subscribe(bikes => {
      this.bikes = bikes;
    });
  }

  getImageUrl(image: string): string {
    return `http://localhost:5000/assets/items/${image}`;
  }

  navigateToBikeDetails(id: number): void {

    console.log(localStorage.getItem('hello'));

    this.router.navigate([`/bike-details/${id}/view`]);
  }
}
