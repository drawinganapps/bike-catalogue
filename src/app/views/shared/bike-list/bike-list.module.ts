import {NgModule} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import { SearchModule } from '../search/search.module';
import { BikeListContainerComponent } from './bike-list-container/bike-list-container.component';

const routes: Routes = [
  {
    path: 'list',
    component: BikeListContainerComponent
  }
];

@NgModule({
  declarations: [
    BikeListContainerComponent
  ],
  imports: [
    CommonModule,
    SearchModule,
    NgOptimizedImage,
    MatButtonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    BikeListContainerComponent
  ]
})
export class BikeListModule {
}
