import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BikeDetailContainerComponent} from './bike-detail-container/bike-detail-container.component';
import {RouterModule, Routes} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";

const routes: Routes = [
  {
    path: 'view',
    component: BikeDetailContainerComponent
  }
];

@NgModule({
  declarations: [
    BikeDetailContainerComponent
  ],
  exports: [
    BikeDetailContainerComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    RouterModule.forChild(routes)
  ]
})
export class BikeDetailModule {
}
