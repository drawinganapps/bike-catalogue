import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeDetailContainerComponent } from './bike-detail-container.component';

describe('BikeDetailContainerComponent', () => {
  let component: BikeDetailContainerComponent;
  let fixture: ComponentFixture<BikeDetailContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BikeDetailContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BikeDetailContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
