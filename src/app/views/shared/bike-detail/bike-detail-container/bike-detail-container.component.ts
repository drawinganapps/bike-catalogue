import {Component, OnInit} from '@angular/core';
import {BikeModel} from "../../../../core/bikes/models/bike.model";
import {switchMap, take} from "rxjs";
import {DummyData} from "../../../../core/dummy-data";
import {BikeService} from "../../../../core/bikes/service/bike.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-bike-detail-container',
  templateUrl: './bike-detail-container.component.html',
  styleUrls: ['./bike-detail-container.component.scss']
})
export class BikeDetailContainerComponent implements OnInit {

  bike: BikeModel;
  protected readonly DummyData = DummyData;

  constructor(private activatedRoute: ActivatedRoute, private bikeService: BikeService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.pipe(
      take(1),
      switchMap(params => {
        const bikeId = params['id'];
        return this.bikeService.findBikeById(bikeId);
      })
    ).subscribe((bike) => {
      this.bike = bike;
    });
  }

  loadImageUrl(url: string): string {
    return `http://localhost:5000/assets/items/${url}`;
  }

  onNavigateBack(): void {
    this.router.navigate(['/bikes/list']);
  }

}
