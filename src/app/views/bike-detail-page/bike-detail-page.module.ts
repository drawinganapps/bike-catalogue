import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BikeDetailPageContainerComponent} from './bike-detail-page-container/bike-detail-page-container.component';
import {HeaderModule} from "../shared/header/header.module";
import {RouterModule, Routes} from "@angular/router";
import {BikeDetailModule} from "../shared/bike-detail/bike-detail.module";

const routes: Routes = [
  {
    path: '',
    component: BikeDetailPageContainerComponent
  }
];

@NgModule({
  declarations: [
    BikeDetailPageContainerComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    RouterModule.forChild(routes),
    BikeDetailModule
  ]
})
export class BikeDetailPageModule {
}
