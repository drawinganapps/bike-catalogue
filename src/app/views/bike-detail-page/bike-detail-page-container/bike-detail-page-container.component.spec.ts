import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeDetailPageContainerComponent } from './bike-detail-page-container.component';

describe('BikeDetailPageContainerComponent', () => {
  let component: BikeDetailPageContainerComponent;
  let fixture: ComponentFixture<BikeDetailPageContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BikeDetailPageContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BikeDetailPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
