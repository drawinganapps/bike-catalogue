import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-bike-detail-page-container',
  templateUrl: './bike-detail-page-container.component.html',
  styleUrls: ['./bike-detail-page-container.component.scss']
})
export class BikeDetailPageContainerComponent {

  constructor() {
  }
}
