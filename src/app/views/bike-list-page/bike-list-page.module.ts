import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikeListPageContainerComponent } from './bike-list-page-container/bike-list-page-container.component';
import { RouterModule, Routes } from '@angular/router';
import { BikeListModule } from '../shared/bike-list/bike-list.module';
import {HeaderModule} from "../shared/header/header.module";

const routes: Routes = [
  {
    path: '',
    component: BikeListPageContainerComponent
  }
];

@NgModule({
  declarations: [
    BikeListPageContainerComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        BikeListModule,
        HeaderModule
    ]
})
export class BikeListPageModule { }
