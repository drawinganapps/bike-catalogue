import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeListPageContainerComponent } from './bike-list-page-container.component';

describe('BikeListPageContainerComponent', () => {
  let component: BikeListPageContainerComponent;
  let fixture: ComponentFixture<BikeListPageContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BikeListPageContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BikeListPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
