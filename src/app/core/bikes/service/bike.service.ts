import {Injectable} from '@angular/core';
import {BikeModel} from "../models/bike.model";
import {DummyData} from "../../dummy-data";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BikeService {
  constructor() {
  }

  getAllBikes(): Observable<BikeModel[]> {
    return of(DummyData.bikes);
  }

  findBikes(query: string): Observable<BikeModel[]> {
    if (!query || query === '') {
      return this.getAllBikes();
    }

    const bikes = DummyData.bikes.filter(bike => {
      console.log(query);
      return bike.name.toLowerCase().includes(query.toLowerCase());
    });
    return of(bikes);
  }

  findBikeById(id: number): Observable<BikeModel> {
    const bike: BikeModel = DummyData.bikes.find(bike => {
      return Number(bike.id) === Number(id);
    });
    return of(bike);
  }
}
