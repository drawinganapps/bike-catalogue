export enum BrandEnum {
  YAMAHA = 'Yamaha',
  KAWASAKI = 'Kawasaki',
  HONDA = 'Honda',
}
