import {BikeModel} from "./bikes/models/bike.model";
import {BrandEnum} from "./bikes/models/brand.enum";

export class DummyData {
  static bikes: BikeModel[] = [
    {
      id: 1,
      name: 'XSR 900',
      brand: BrandEnum.YAMAHA,
      picture: 'xsr.png',
      price: 60000000,
      year: 2023
    },
    {
      id: 2,
      name: 'Ninja 25 R',
      brand: BrandEnum.KAWASAKI,
      picture: 'ninja.png',
      price: 120000000,
      year: 2022
    },
    {
      id: 3,
      name: 'Aerox 155',
      brand: BrandEnum.YAMAHA,
      picture: 'aerox.png',
      price: 20000000,
      year: 2020
    },
    {
      id: 4,
      name: 'Scoopy',
      brand: BrandEnum.YAMAHA,
      picture: 'scoopy.png',
      price: 22000000,
      year: 2018
    },
    {
      id: 5,
      name: 'R 15',
      brand: BrandEnum.YAMAHA,
      picture: 'r15.png',
      price: 32000000,
      year: 2021
    },
    {
      id: 6,
      name: 'X-Max ABS',
      brand: BrandEnum.YAMAHA,
      picture: 'xmax.png',
      price: 62000000,
      year: 2022
    },
    {
      id: 7,
      name: 'XSR 900',
      brand: BrandEnum.YAMAHA,
      picture: 'xsr.png',
      price: 60000000,
      year: 2023
    },
    {
      id: 8,
      name: 'Ninja 25 R',
      brand: BrandEnum.KAWASAKI,
      picture: 'ninja.png',
      price: 120000000,
      year: 2022
    },
    {
      id: 9,
      name: 'Aerox 155',
      brand: BrandEnum.YAMAHA,
      picture: 'aerox.png',
      price: 20000000,
      year: 2020
    },
    {
      id: 10,
      name: 'Scoopy',
      brand: BrandEnum.YAMAHA,
      picture: 'scoopy.png',
      price: 22000000,
      year: 2018
    },
    {
      id: 11,
      name: 'R 15',
      brand: BrandEnum.YAMAHA,
      picture: 'r15.png',
      price: 32000000,
      year: 2021
    },
    {
      id: 12,
      name: 'X-Max ABS',
      brand: BrandEnum.YAMAHA,
      picture: 'xmax.png',
      price: 62000000,
      year: 2022
    }
  ];

  static dummyText: string = `Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,`;
}
