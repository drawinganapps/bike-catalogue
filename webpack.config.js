const { shareAll, withModuleFederationPlugin } = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({
  name: 'mfe1',
  filename: "remoteEntry.js",
  exposes: {
    './BikeListModule': './src/app/views/shared/bike-list/bike-list.module.ts',
    './BikeDetailModule': './src/app/views/shared/bike-detail/bike-detail.module.ts'
  },
  shared: {
    ...shareAll({ singleton: true, strictVersion: true, requiredVersion: 'auto' }),
  },

});
